int istart;
int nfiles;
//for( int ii = 0; ii < nfiles; ii++ ){}
//def nom_runs = [ 5359, 5376, 5391, 5404, 5416, 5428, 5440, 5452, 5464, 5477, 5505];
//def nom_runs = [ 5452, 5464, 5477, 5505];
//def nom_runs = [ 5505];
def nom_runs = [ 5404, 5416, 5464]; // Interesting ones
if( args.size() == 0 ){
  istart = 0;
  nfiles = nom_runs.size();
}
else{
  istart = args[0] as int;
  nfiles = args[1] as int;
}
//for( ii in nom_runs ){
for( ii = 0; ii < nfiles; ii++ ){
	int irunnum = istart + ii;	
	String srunnum = irunnum as String;
  println srunnum;
	run(new File("pmtAnalysis.groovy"), srunnum);
}
int iend = istart + nfiles - 1;
println(" Done analyzing run numbers : " + istart + " - " + iend);

